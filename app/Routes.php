<?php

return [
    '/' => [
        'GET' => 'Home:index'
    ],
    '/details' => [
        'GET' => 'Details:index'
    ]
];