<?php
namespace app\controllers;

use \fw\Controller as BaseController;

class Home extends BaseController {
    public function index() {
        $this->render('Home', ['name' => 'James']);
    }
}