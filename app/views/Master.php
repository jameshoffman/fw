<html>
<head>
    <title><?=$this->e($title)?></title>
</head>
<body>

<h1>👉 <?=$this->e($title)?></h1>

<?=$this->section('content')?>

</body>
</html>