<?php

require('plates/Engine.php');
require('plates/Template/Directory.php');
require('plates/Template/FileExtension.php');
require('plates/Template/Folders.php');
require('plates/Template/Functions.php');
require('plates/Template/Data.php');
require('plates/Template/Template.php');
require('plates/Template/Name.php');
require('plates/Template/Func.php');
require('plates/Template/Folder.php');

spl_autoload_register(function ($class) {
    $file = '../' . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';

    if(file_exists($file)) {
        require_once($file);
    }
});