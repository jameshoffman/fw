<?php
namespace fw;

class Model implements \JsonSerializable {
    protected $data;

    public function __construct($data) { 
        $this->data = $data;
    }

    public static function init($data) {
        return new Model($data);
    }

    public function __get($key) {
        return $this->data[$key];
    }

    public function __set($key, $value) {
        $this->data[$key] = $value;
    }

    public function jsonSerialize() {
        return $this->data;
    }
}