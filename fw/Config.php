<?php
namespace fw;

class Config {
    private static $values = [];

    public static function get($key) {
        if (count(static::$values) == 0 && file_exists('../app/Config.php')) {
            static::$values = include_once('../app/Config.php');
        }

        return static::$values[$key];
    }
}