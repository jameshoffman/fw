# fw

## Dev

- Start dev server
```
cd webroot && php -S 0.0.0.0:8080
```
- Test deployment setup using the Vagrant VM
```
vagrant up
vagrant halt
```

## Deploy

- Set the webserver's document root to `webroot`
- Enable `AllowOverride All` in Apache config